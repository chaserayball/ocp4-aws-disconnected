Openshift Installation on AWS Existing Private VPC with Mirrored Registry
==


**Objective**

To install OCP private cluster v4.6, into an existing VPC on Amazon Web Services (AWS).

**Context**

The installation program by default provisions all the underlying infrastructure. However when the target is an existing VPC, the installer will provision the part of the infrastructure that strictly involves the OCP cluster.

The OCP installer provisions:

- EC2 instances for OCP nodes

- Private Route53 zone

- Elastic Load Balancers


The existing VPC shall have:

- Internet gateways

- NAT gateways

- Subnets

- Route tables

- VPCs

- VPC DHCP options

- VPC endpoints

*NOTE: The VPC’s CIDR block must contain the Networking.MachineCIDR range, which is the IP address pool for cluster machines. The VPC must not use the kubernetes.io/cluster/.: owned tag. You must enable the enableDnsSupport and enableDnsHostnames attributes in your VPC so that the cluster can use the Route 53 zones that are attached to the VPC to resolve cluster’s internal DNS records. See DNS Support in Your VPC in the AWS documentation.*



**OCP Architecture**

![OCP Architecture](media/ocp-architecture.png)

**OCP VPC Topology**

![OCP VPC Topology](media/private-disconnected.png)


`aws cloudformation create-stack --stack-name vpc --template-body file://templates/0-vpc/cluster-vpc-cloudformation-template.yaml --parameters file://templates/0-vpc/cluster-vpc-parameters.json`

`aws cloudformation create-stack --stack-name securitygroup --template-body file://templates/1-securitygroups/cluster-security-groups-cloudformation-template.yaml --parameters file://templates/1-securitygroups/cluster-security-groups-parameters.json`

`aws cloudformation create-stack --stack-name bastion --template-body file://templates/2-bastion/cluster-bastion-ec2-instance-cloudformation-template.yaml --parameters file://templates/2-bastion/cluster-bastion-ec2-instance-parameters.json`


**OCP Installation Steps**

- Provision bastion host

- Configure mirrored installation registry

- Create installation directory and update install-config.yaml

- Provision OCP cluster

----
