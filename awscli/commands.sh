Variables:
export VpcCidr="10.0.0.0/16"
export Subnet0Cidr="10.0.0.0/24"
export Subnet1Cidr="10.0.1.0/24"
export Subnet2Cidr="10.0.2.0/24"
export DNSServerIp="10.0.3.10"
export ClusterDomainName="top.job"
export SubnetPublicCidr="10.0.3.0/24"
export AvailabilityZone0="eu-west-1a"
export AvailabilityZone0Id="euw1-az3"
export AvailabilityZone1="eu-west-1b"
export AvailabilityZone1Id="euw1-az1"
export AvailabilityZone2="eu-west-1c"
export AvailabilityZone2Id="euw1-az2"

## 0 -- Creating VPC

# 0.1 - Create VPC
aws ec2 create-vpc --cidr-block ${VpcCidr}
export VpcId=vpc-016051cb3c5cfd990
aws ec2 modify-vpc-attribute --vpc-id ${VpcId} --enable-dns-hostnames "{\"Value\":true}"
aws ec2 modify-vpc-attribute --vpc-id ${VpcId} --enable-dns-support "{\"Value\":true}"

aws ec2 describe-vpc-attribute --attribute enableDnsHostnames --vpc-id  ${VpcId}
aws ec2 describe-vpc-attribute --attribute enableDnsSupport --vpc-id  ${VpcId}

# 0.2 - Create DHCP Options
aws ec2 create-dhcp-options --dhcp-configuration "Key=domain-name-servers,Values=${DNSServerIp}" "Key=domain-name,Values=${ClusterDomainName}"
export DhcpOptionsId=dopt-09fbc4d2464e485a9
aws ec2 associate-dhcp-options --dhcp-options-id ${DhcpOptionsId} --vpc-id ${VpcId}

# 0.3 - Create Public Subnet (1x)
aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${SubnetPublicCidr} --availability-zone-id ${AvailabilityZone0Id}
export PublicSubnetId=subnet-0dcd5593f0e9f59dc
aws ec2 create-internet-gateway
export InternetGatewayId=igw-0b4bc21b502980a02
aws ec2 allocate-address --domain vpc
export EipAllocationId=eipalloc-0552772859ca4adc7
aws ec2 create-nat-gateway --subnet-id ${PublicSubnetId} --allocation-id ${EipAllocationId}
aws ec2 attach-internet-gateway --vpc-id ${VpcId} --internet-gateway-id ${InternetGatewayId}
aws ec2 create-route-table --vpc-id ${VpcId}
export PublicRouteTableId=rtb-06b7935ff10ac06b7
aws ec2 create-route --route-table-id ${PublicRouteTableId} --destination-cidr-block 0.0.0.0/0 --gateway-id ${InternetGatewayId}
aws ec2 describe-route-tables --route-table-id ${PublicRouteTableId}
aws ec2 describe-subnets --filters "Name=vpc-id,Values=${VpcId}" --query 'Subnets[*].{ID:SubnetId,CIDR:CidrBlock}'
aws ec2 associate-route-table --subnet-id ${PublicSubnetId} --route-table-id ${PublicRouteTableId}
aws ec2 modify-subnet-attribute --subnet-id ${PublicSubnetId} --map-public-ip-on-launch

# 0.4 - Create Private Subnets (3x)
aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet0Cidr} --availability-zone-id ${AvailabilityZone0Id}
export PrivateSubnet0Id=subnet-08979dd685db45e5d
aws ec2 create-route-table --vpc-id ${VpcId}
export PrivateRouteTable0Id=rtb-0dda1eccb84dd2bfb
aws ec2 create-route --route-table-id ${PrivateRouteTable0Id} --destination-cidr-block ${VpcCidr} --gateway-id ${InternetGatewayId}
aws ec2 associate-route-table --subnet-id ${PrivateSubnet0Id} --route-table-id ${PrivateRouteTable0Id}

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet1Cidr} --availability-zone-id ${AvailabilityZone1Id}
export PrivateSubnet1Id=subnet-0326ac76a565bce3c
aws ec2 create-route-table --vpc-id ${VpcId}
export PrivateRouteTable1Id=rtb-0cd2bcbcb28ece6e7
aws ec2 create-route --route-table-id ${PrivateRouteTable1Id} --destination-cidr-block ${VpcCidr} --gateway-id ${InternetGatewayId}
aws ec2 associate-route-table --subnet-id ${PrivateSubnet1Id} --route-table-id ${PrivateRouteTable1Id}

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet2Cidr} --availability-zone-id ${AvailabilityZone2Id}
export PrivateSubnet2Id=subnet-0cfb950affce42f3a
aws ec2 create-route-table --vpc-id ${VpcId}
export PrivateRouteTable2Id=rtb-03c70e5e2b19c7537
aws ec2 create-route --route-table-id ${PrivateRouteTable2Id} --destination-cidr-block ${VpcCidr} --gateway-id ${InternetGatewayId}
aws ec2 associate-route-table --subnet-id ${PrivateSubnet2Id} --route-table-id ${PrivateRouteTable2Id}


## 1 -- Creating Security Groups

# 1.0 - Creating BastionSecurityGroup
aws ec2 create-security-group --group-name "BastionSecurityGroup" --description "BastionSecurityGroup" --vpc-id ${VpcId}
export BastionSecurityGroupId=sg-0537abcb30e5b63ea
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=icmp,FromPort=0,ToPort=0,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=udp,FromPort=53,ToPort=53,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=53,ToPort=53,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=6443,ToPort=6443,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=22623,ToPort=22623,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=19531,ToPort=19531,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=5000,ToPort=5000,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=8080,ToPort=8080,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-egress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=-1,FromPort=0,ToPort=65535,IpRanges='[{CidrIp=0.0.0.0/0}]'


# 1.1 - Creating ClusterSecurityGroup
aws ec2 create-security-group --group-name "ClusterSecurityGroup" --description "ClusterSecurityGroup" --vpc-id ${VpcId}
export ClusterSecurityGroupId=sg-0b6f58d692484ae46
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=icmp,FromPort=0,ToPort=0,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=udp,FromPort=53,ToPort=53,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=53,ToPort=53,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=6443,ToPort=6443,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=22623,ToPort=22623,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=19531,ToPort=19531,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=5000,ToPort=5000,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=8080,ToPort=8080,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-egress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=-1,FromPort=0,ToPort=65535,IpRanges='[{CidrIp=0.0.0.0/0}]'


aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=2049,ToPort=2049,IpRanges='[{CidrIp=10.0.0.0/16}]'

# in case there is a proxy
aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=3128,ToPort=3128,IpRanges='[{CidrIp=10.0.0.0/16}]'


# 1.2 - Creating MasterSecurityGroup
aws ec2 create-security-group --group-name "MasterSecurityGroup" --description "MasterSecurityGroup" --vpc-id ${VpcId}
export MasterSecurityGroupId=sg-08faa1b36f2f9b37e
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=6443,ToPort=6443,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=22623,ToPort=22623,IpRanges='[{CidrIp=10.0.0.0/16}]'
aws ec2 authorize-security-group-egress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=-1,FromPort=0,ToPort=65535,IpRanges='[{CidrIp=0.0.0.0/0}]'

# 1.3 - Creating WorkerSecurityGroup
aws ec2 create-security-group --group-name "WorkerSecurityGroup" --description "WorkerSecurityGroup" --vpc-id ${VpcId}
export WorkerSecurityGroupId=sg-07dc8b67613449d25

# 1.4 - Master Ingresses Groups
# MasterIngressEtcd
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=2379-2380
# MasterIngressVxlan
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=udp --port=4789
# MasterIngressWorkerVxlan
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=udp --port=4789
# MasterIngressInternal
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=9000-9999
# MasterIngressWorkerInternal
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=9000-9999
# MasterIngressKube
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=10250-10259
# MasterIngressWorkerKube
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=10250-10259
# MasterIngressIngressServices
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=30000-32767
# MasterIngressWorkerIngressServices
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=30000-32767

# 1.5 - Worker Ingresses Groups
# WorkerIngressVxlan
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=udp --port=4789
# WorkerIngressWorkerVxlan
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=udp --port=4789
# WorkerIngressInternal
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=9000-9999
# WorkerIngressWorkerInternal
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=9000-9999
# WorkerIngressKube
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=10250
# WorkerIngressWorkerKube
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=10250
# WorkerIngressIngressServices
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=30000-32767
# WorkerIngressWorkerIngressServices
aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=30000-32767


## 3 -- Provision external bastion
aws ec2 create-key-pair --key-name ocpkey --query 'KeyMaterial' --output text > ocpkey.pem
chmod 400 ocpkey.pem

cat > bastion-mapping.json << EODBastion
[
    {
       "DeviceName" : "/dev/sda1",
       "Ebs": { "VolumeSize" : 50, "DeleteOnTermination": false }
    },
    {
       "DeviceName" : "/dev/sdb",
       "Ebs": { "VolumeSize" : 150, "DeleteOnTermination": false }
    }
]
EODBastion




aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name ocpkey --block-device-mappings file://bastion-mapping.json --subnet-id ${PublicSubnetId} --security-group-ids ${BastionSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.3.10

aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name ocpkey --block-device-mappings file://bastion-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${BastionSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.100

## 4 -- Provision bastion

cat > bastion-mapping.json << EODBastion
[
    {
        "DeviceName" : "/dev/sda1",
        "Ebs": { "VolumeSize" : 50, "DeleteOnTermination": false }
    },
    {
        "DeviceName": "/dev/sdb",
        "Ebs": { "VolumeSize" : 100, "DeleteOnTermination": false }
    }
]
EODBastion

aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name OcpKeyPair --block-device-mappings file://bastion-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${BastionSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.5 --associate-public-ip-address


## 5 -- Provision bootstrap
cat > bootstrap-mapping.json << EODBootstrap
[
    {
        "DeviceName" : "/dev/sda1",
        "Ebs": { "VolumeSize" : 120, "DeleteOnTermination": false }
    }
]
EODBootstrap

cat > bootstrap-user-data << EODBootstrapUserData
{
	"ignition": {
		"config": {
			"replace": {
				"source": "http://10.0.0.5:8080/bootstrap.ign",
				"verification": {}
			}
		},
		"timeouts": {},
		"version": "2.2.0"
	},
	"networkd": {},
	"passwd": {},
	"storage": {},
	"systemd": {}
}
EODBootstrapUserData

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type i3.large --key-name OcpKeyPair --subnet-id ${PrivateSubnet0Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.9 --user-data file://bootstrap-user-data  > .bootstrap-creation.log


## 6 -- Provision masters
cat > master-mapping.json << EODMaster
[
    {
        "DeviceName" : "/dev/xvda",
        "Ebs": { "VolumeSize" : 200, "VolumeType": "gp2", "DeleteOnTermination": false }
    }
]
EODMaster

cat > master-user-data << EODMasterpUserData
{
	"ignition": {
		"config": {
			"append": [{
				"source": "https://api-int.ocpcluster.testo.labo:22623/config/master",
				"verification": {}
			}]
		},
		"security": {
			"tls": {
				"certificateAuthorities": [{
					"source": "data:text/plain;charset=utf-8;base64,LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURFRENDQWZpZ0F3SUJBZ0lJRlZuZUd2Wms5UGN3RFFZSktvWklodmNOQVFFTEJRQXdKakVTTUJBR0ExVUUKQ3hNSmIzQmxibk5vYVdaME1SQXdEZ1lEVlFRREV3ZHliMjkwTFdOaE1CNFhEVEl3TVRBeU1EQXhORGd3TjFvWApEVE13TVRBeE9EQXhORGd3TjFvd0pqRVNNQkFHQTFVRUN4TUpiM0JsYm5Ob2FXWjBNUkF3RGdZRFZRUURFd2R5CmIyOTBMV05oTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFwMmx4SkZBc001a3EKSStQdHRLQlhnYThUejYrQ1NSc1dPcVRRTy9GbSsvTEEyaVdEd3FQQkpjeXNUYitST2d1U3UzNGFYWGJKWDRRQQpsZjJBUVpnNUlZNlJTMmdwczNmbUZOdldodTFNS0s2VHVYNnF2WlIwOTBySmNCREtjYlgxbEZWcGxlL3NMbGNGCkpTRzNLcUZaN2xVM1BoZ2l4dnhMaWNmTkxDS1kzcEtac1o0bG9ocWtHTDdWUFpqVXJUS1RnVWlYOU1GNVpWRFQKa0FZQTdNQUVJMi8zaXNJSDR4YVZQcmpkbTVXYjNVUk9oeGVOb3lLb1J5RzcxbEJqTDE5Y0JNMmQ0UXl0Zm1jMgppcGR1QjdWYmNmVENPTHhTbkdheWhibjJGMjRJZ3B3M3lwUGtQeDR6Zkk3N0YvVHZhNkRqYjNvdE1pbTJMNlRDCllKWFg4aWdQWndJREFRQUJvMEl3UURBT0JnTlZIUThCQWY4RUJBTUNBcVF3RHdZRFZSMFRBUUgvQkFVd0F3RUIKL3pBZEJnTlZIUTRFRmdRVWltMFhBWUhkdkwzMm1seDlySkxLVk1NVFlrMHdEUVlKS29aSWh2Y05BUUVMQlFBRApnZ0VCQUFRYzlBU2pQckduNEx1dEUwd1I4SkVqRE82L2ZhZzA1U2JLUTF6MmwrdXlRY2VqY0kzKzh6dXp3UGQwClFmRGI1L3FXOXdsTlU4RHJtYTdOc05UUkxidXhIWHRobUFlTWU3ejI2Tk5qQnVidU04SGxKK1dCUUNjMjVnSEcKN0dUclpDWTVZcFVQWDdwWTVUbnRQMm4zQnhCOENQWU9vK1k1OExpWGRwYWF4K0VuZC9TbVRzK0hnTjd6UEd4QQpET2pFejUyU3JrWndWM2x2Zk9DaEZacVZmaFd0SkJoQnExclZHZEw2VGVZV3J3dWhzVWxiUDliU1ZGNkdhT3grCnlKcm5TcFVTN21qdFAwUFR6OEFrQWh4ODl0MzZyZnVFcmV2cm5pZ21lcmhQcW1Xd1FoS3R6YnJsZ2p0RGw1NFIKZkhBVEhJTW1NMFMvMnpQWG55TG42aU1EbDJJPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==",
					"verification": {}
				}]
			}
		},
		"timeouts": {},
		"version": "2.2.0"
	},
	"networkd": {},
	"passwd": {},
	"storage": {},
	"systemd": {}
}
EODMasterpUserData

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.4xlarge --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.10 --user-data file://master-user-data > .master-0-creation.log

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.4xlarge  --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet1Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone1} --private-ip-address 10.0.1.10 --user-data file://master-user-data > .master-1-creation.log

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.4xlarge  --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet2Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone2} --private-ip-address 10.0.2.10 --user-data file://master-user-data > .master-2-creation.log


eval "$(ssh-agent -s)"
ssh-add /home/ec2-user/.ssh/ocpkey




## 7 -- Provision worker
cat > worker-mapping.json << EODWorker
[
    {
        "DeviceName" : "/dev/xvda",
        "Ebs": { "VolumeSize" : 120, "DeleteOnTermination": false }
    }
]
EODWorker

cat > worker-user-data << EODWorkerpUserData
{
  "ignition":
    {
      "config":{"replace":{"source":"http://10.0.0.5:8080/worker.ign","verification":{}}},
      "timeouts":{}
      "version":"2.2.0"
    },
  "networkd":{},
  "passwd":{},
  "storage":{},
  "systemd":{}
}
EODWorkerpUserData

base64 master-user-data > master-user-data-base64.txt

aws ec2 run-instances --image-id ami-0d4839574724ed3fa --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://worker-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids [${WorkerSecurityGroupId}, ${ClusterSecurityGroupId}] --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.20 --user-data file://worker-user-data

aws ec2 run-instances --image-id ami-0d4839574724ed3fa --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://worker-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids [${WorkerSecurityGroupId}, ${ClusterSecurityGroupId}] --placement AvailabilityZone=${AvailabilityZone1} --private-ip-address 10.0.1.20 --user-data file://worker-user-data

aws ec2 run-instances --image-id ami-0d4839574724ed3fa --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://worker-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids [${WorkerSecurityGroupId}, ${ClusterSecurityGroupId}] --placement AvailabilityZone=${AvailabilityZone2} --private-ip-address 10.0.2.20 --user-data file://worker-user-data








Peteka88-Amsterdam!


#!/usr/bin/env bash

##############################################################
# UPDATE TO MATCH THE ENVIRONMENT
##############################################################


# OCP RELEASE AND VERSION ###
OCP_RELEASE_PATH=ocp
OCP_SUBRELEASE=4.4.17

RHCOS_RELEASE=4.4
RHCOS_IMAGE_BASE=4.4.17-x86_64

# PULL SECRET - cloud.openshift.com ###
RHEL_PULLSECRET='redhat-registry-pullsecret.json'

# OCP CLUSTER NAME AND DOMAIN ###
CLUSTER_NAME=ocpcluster
DOMAINNAME=testo.labo


# LOCAL REGISTRY FOR DISCONNECTED INSTALLATION ##
AIRGAP_REG="externalbastion.testo.labo"

# STORAGE DEVICE FOR INTERNA CLUSTER REGISTRY
NFS_DEV=xvdb

## THESE VARIABLES SHOULD BE UPDATED TO CONFIGURE DNS AND PROXY SERVERS ##
BASTION_IP="10.0.0.5"
BASTION_IP_FIRST_THREE_OCTET_INVERTED="0.0.10"
BASTION_IP_LAST_OCTET="5"
VPC_NETMASK="16"
BOOTSTRAP_IP="10.0.0.9"
BOOTSTRAP_IP_LAST_OCTET="9"
MASTER_0_IP="10.0.0.10"
MASTER_0_IP_FIRST_THREE_OCTET_INVERTED="0.0.10"
MASTER_0_IP_LAST_OCTET="10"
MASTER_1_IP="10.0.1.10"
MASTER_1_IP_FIRST_THREE_OCTET_INVERTED="1.0.10"
MASTER_1_IP_LAST_OCTET="10"
MASTER_2_IP="10.0.2.10"
MASTER_2_IP_FIRST_THREE_OCTET_INVERTED="2.0.10"
MASTER_2_IP_LAST_OCTET="10"
WORKER_0_IP="10.0.0.20"
WORKER_0_IP_LAST_OCTET="20"
WORKER_1_IP="10.0.1.20"
WORKER_1_IP_LAST_OCTET="20"
WORKER_2_IP="10.0.2.20"
WORKER_2_IP_LAST_OCTET="20"
-

cat > test.json << EODTest
[
    {
        "DeviceName" : "/dev/sda1",
        "Ebs": { "VolumeSize" : 100, "DeleteOnTermination": true }
    }
]
EODTest

cat > test-user-data << EODTestUserData
curl http://10.0.0.5:8080/worker.ign > mio
EODTestUserData


aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name OcpKeyPair --block-device-mappings file://test.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.19 --user-data file://test-user-data

aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.small --key-name OcpKeyPair --subnet-id subnet-0d0bf53c10f8c2991 --security-group-ids sg-048c464308789476c --placement AvailabilityZone=eu-west-1a --private-ip-address 10.0.3.19 --associate-public-ip-address


for 22 80
firewall-cmd --permanent --add-port=5000/tcp
firewall-cmd --permanent --add-port=5000/udp
firewall-cmd --reload

aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=,ToPort=5910,IpRanges='[{CidrIp=0.0.0.0/0}]'

aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp=0.0.0.0/0}]'
aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,IpRanges='[{CidrIp=0.0.0.0/0}]'


34.245.12.188:5901

quay.io/opencloudio/common-web-ui
quay.io/opencloudio/ibm-cert-manager-operator
quay.io/opencloudio/icp-kibana-oss
quay.io/opencloudio/icp-filebeat-oss
quay.io/opencloudio/ibm-ingress-nginx-operator




# create elastic fielsystem
aws efs create-file-system --performance-mode generalPurpose --throughput-mode bursting --encrypted --tags Key=Name,Value=icp-efs

export efs-id=
aws describe-file-systems --file-system-id ${efs-id}


aws ec2 create-volume --volume-type gp2 --size 10 --availability-zone eu-west-1a
aws ec2 create-volume --volume-type gp2 --size 200 --availability-zone eu-west-1b
aws ec2 create-volume --volume-type gp2 --size 200 --availability-zone eu-west-1c

export VolumeId=vol-0dce1f18a90973f80
export InstanceId=i-0384d97d147321f77
aws ec2 attach-volume --volume-id ${VolumeId} --instance-id ${InstanceId} --device /dev/xvdd


curl -u admin:admin https://`(hostname)`:5000/v2/ocp4/openshift4/tags/list | jq
curl -u rcardona@redhat.com:Scar8153@ https://registry.redhat.io/v2/ocp4/openshift4/tags/list | jq

imageContentSources:
- mirrors:
  - ip-10-0-3-10.top.job:5000/ocp4/openshift4
  source: quay.io/openshift-release-dev/ocp-release
- mirrors:
  - ip-10-0-3-10.top.job:5000/ocp4/openshift4
  source: quay.io/openshift-release-dev/ocp-v4.0-art-dev

  echo -n 'admin:admin' | base64 -w0
  YWRtaW46YWRtaW4=

pullSecret: '{"auths":{"ip-10-0-3-10.eu-west-1.compute.internal:5000": {"auth": "YWRtaW46YWRtaW4=","email": "you@example.com"}}}'
